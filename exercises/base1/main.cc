// base for several exercises
#include "lest.hpp"
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using namespace std::literals;

const lest::test myTests[] =
{
    CASE("dummy 1")
    {
        int result = 1;

        EXPECT(result == 1);
    },
    CASE("dummy -1")
    {
        int result = -1;

        EXPECT(result == -1);
    },
};

std::vector args = {"-p"s, "-a"s};

int main()
{
    int status = lest::run(myTests, args, std::cerr);
    if (status == 0)
    {
        cout << "Tests successful\n";
    } else {
        cout << "Tests failed\n";
    }

    return 0;
}
