// some books
static Book b1 = {101
                  , "Bjarne Stroustrup", "The C++ Programming Language"
                  , "AWL", "0-201-88954-4", 34.28, 0.675};
static Book b2 = {121
                  , "Scott Meyers", "Effective C++"
                  , "AWL", "0-201-92488-9", 28.95, 0.355};
static Book b3 = {141
                  , "Herb Sutter", "Exceptional C++"
                  ,"AWL", "0-201-61562-2", 31.70, 0.405};
static Book b4 = {171
                  , "Stan Lippman, Josee Lajoie", "C++ Primer"
                  , "AWL", "0-201-82470-1", 22.35, 0.829};
