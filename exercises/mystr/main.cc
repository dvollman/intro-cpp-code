// Exercise new/delete

#include "allocmock.hh"

#include "mystr.hh"
#include <iostream>
#include <string>
#include <vector>

#include "lest.hpp"
#include <sstream>

using std::cout;
using exercise::MyString;
using namespace std::literals;

//#define MYSTRING_DEF_CTOR 1
//#define MYSTRING_SET 1
//#define MYSTRING_COPY_CTOR 1
//#define MYSTRING_COPY_ASS 1
//#define MYSTRING_MOVE_CTOR 1
//#define MYSTRING_MOVE_ASS 1
//#define MYSTRING_APPEND_OP 1
//#define MYSTRING_OUT_OP 1
#include "testimpl.hh"

namespace
{
using namespace exercise;
using std::string;
std::string testHelperGetString(MyString const &ms)
{
    std::ostringstream os;
#ifdef MYSTRING_OUT_OP
    os << ms;
#else
    ms.print(os);
#endif
    return os.str();
}
std::vector lestArgs = {"-p"s, "-a"s};
const lest::test myTests[] =
{
    CASE("Check constructor")
    {
        string s = "TeSt";
        MyString ms = s;

        EXPECT(ms.size() == 4u);
        EXPECT(testHelperGetString(ms) == s);
    },
    CASE("Check print()")
    {
        string s{"TestPrint"s};
        MyString ms{s};
        std::ostringstream os;
#ifdef MYSTRING_OUT_OP
        os << ms;
#else
        ms.print(os);
#endif
        EXPECT(s == os.str());
    },
#ifdef MYSTRING_SET
    CASE("Check set()")
    {
        string s = "TeSt";
#ifdef MYSTRING_DEF_CTOR
        MyString ms;
#else
        std::string empty;
        MyString ms{empty};
#endif
        ms.set(s);

        EXPECT(ms.size() == 4u);
        EXPECT(testHelperGetString(ms) == s);
    },
#endif
#ifdef MYSTRING_DEF_CTOR
    CASE("Check default ctor()")
    {
        MyString ms{};
        EXPECT(testHelperGetString(ms).empty());
    },
#endif
#ifdef MYSTRING_APPEND_OP
    CASE("Check append")
    {
        std::string s1 = "Test "s;
        std::string s2 = "String"s;
        MyString ms1 = s1;
        MyString ms2 = s2;
        ms1 += ms2;

        std::ostringstream os;
#ifdef MYSTRING_OUT_OP
        os << ms1;
#else
        ms1.print(os);
#endif
        EXPECT(testHelperGetString(ms1) == s1 + s2);
    },
#endif
};

void print(MyString const &s)
{
#ifdef MYSTRING_OUT_OP
    cout << s;
#else
    s.print(cout);
#endif
    cout << '\n';
}
} // unnamed namespace

int main()
{
    int status = lest::run(myTests, lestArgs, std::cout);
    status += lest::run(allocTests, lestArgs, std::cout);
    if (status == 0)
    {
        cout << "Tests successful\n";
    } else {
        cout << "Tests failed\n";
    }

    std::string s("Hello");
    std::string empty;

#ifdef MYSTRING_DEF_CTOR
    MyString s1{};
#else
    MyString s1{empty};
#endif
    MyString s2(s);
    MyString s3("Hi"s);

    print(s1);
    print(s2);
    print(s3);

#ifdef MYSTRING_SET
    s1.set("C++"s);
    print(s1);
#endif

#ifdef MYSTRING_COPY_CTOR
    MyString s4{s2};
    print(s4);
#endif

#ifdef MYSTRING_COPY_ASS
    s3 = s1;
    print(s3);
#endif

#ifdef MYSTRING_MOVE_CTOR
    MyString s5{std::move(s1)};
    print(s5);
    print(s1);
#endif

#ifdef MYSTRING_MOVE_ASS
    s2 = std::move(s3);
    print(s2);
    print(s3);
#endif

    return 0;
}
