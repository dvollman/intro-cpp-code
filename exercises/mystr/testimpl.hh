namespace exerciseTest
{
// tests for exercise new/delete
#include "allocmock.hh"

typedef std::vector<exerciseTest::AllocMock *, exerciseTest::StackAllocator<exerciseTest::AllocMock *, 10>> AllocVec;
AllocVec AllocMock::allocs;
// just a dummy to set vector's capacity
int sizedAllocMockAllocs{AllocMock::reserveAllocs()};

// we need always at least one alloc
AllocMock globalTopAlloc;
} // namespace exerciseTest

void *operator new(size_t n)
{
    return exerciseTest::AllocMock::getCurrent().alloc(n);
}

void operator delete(void *p) noexcept
{
    exerciseTest::AllocMock::getCurrent().dealloc(p);
}

// for sized deallocation
void operator delete(void *p, long unsigned) noexcept
{
    exerciseTest::AllocMock::getCurrent().dealloc(p);
}

namespace
{
using namespace exercise;
using namespace exerciseTest;
using trompeloeil::_;
using std::string;
const lest::test allocTests[] =
{
    CASE("Test memory char * ctor/dtor")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

            MyString s{"Some Text"s};
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
    CASE("Test string ctor/dtor")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

            MyString s{"Some Text"s};
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#ifdef MYSTRING_DEF_CTOR
    CASE("Test memory default ctor")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

            MyString s;
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#endif
#ifdef MYSTRING_SET
    CASE("Test memory set(string)")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

#ifdef MYSTRING_DEF_CTOR
            MyString s;
#else
            std::string empty;
            MyString s{empty};
#endif
            s.set("Some Text"s);
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#endif
#ifdef MYSTRING_COPY_CTOR
    CASE("Test memory copy ctor")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

            MyString s1("Text1"s);
            MyString s2{s1};
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#endif /* MYSTRING_COPY_ASS */
#ifdef MYSTRING_COPY_ASS
    CASE("Test memory copy assignment")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            ALLOW_CALL(tracer, trackNew(_, _));
            ALLOW_CALL(tracer, trackDelete(_));

            TraceSetter ts{testAlloc, &tracer};

            MyString s1("Text1"s);
            MyString s2{"Other Text"s};
            s1 = s2;
        }

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#endif /* MYSTRING_COPY_ASS */
#if defined(MYSTRING_MOVE_CTOR)
    CASE("Test moved-from size")
    {
        string s{"TestPrint"s};
        MyString ms1{s};
        MyString ms2{std::move(ms1)};

        EXPECT(ms1.size() == 0u);
    },
    CASE("Test moved-from print")
    {
        string s{"TestPrint"s};
        MyString ms1{s};
        MyString ms2{std::move(ms1)};
        std::ostringstream os;
        ms2.print(os);
        EXPECT(s == os.str());

        os.str(""s);
        ms1.print(os);
        EXPECT(os.str().size() == 0u);
    },
    CASE("Test move ctor must not cause any memory calls")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            MyString s1("Text1"s);
            s1.~MyString();
            MyString s2{"Other Text"s};
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                new (&s1) MyString{std::move(s2)};
            }
        }
        // now all acquired resources should be released

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
    CASE("Test move ctor must move ownership (no destruction of moved from)")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            MyString s1("Text1"s);
            s1.~MyString();
            MyString s2{"Other Text"s};
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                new (&s1) MyString{std::move(s2)};
                s2.~MyString();
            }
            // we must restore a MyString object before it's destructor is called
            new (&s2) MyString;
        }
        // now all acquired resources should be released

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
    CASE("Test move ctor of moved from")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            std::byte buf[sizeof(MyString)];
            MyString *tms = nullptr;

            MyString s1("Text1"s);
            MyString s2{std::move(s1)};

            // move moved-from s1 again
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                tms = new (buf) MyString{std::move(s1)};
            }
            // clean-up empty moved-to
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                tms->~MyString();
            }
        }
        // now all acquired resources should be released

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
#endif /* MYSTRING_MOVE_CTOR */
#if defined(MYSTRING_MOVE_ASS)
    CASE("Test moved-from print")
    {
        string s{"TestPrint"s};
        MyString ms1{s};
        MyString ms2;
        ms2 = std::move(ms1);
        std::ostringstream os;
        ms2.print(os);
        EXPECT(s == os.str());

        os.str(""s);
        ms1.print(os);
        EXPECT(os.str().size() == 0u);
    },
    CASE("Test move assignment w/ empty target must not cause any memory calls")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            std::byte buf[sizeof(MyString)];
            MyString s1("Text1"s);
            MyString *s2 = new (buf) MyString{"Other Text"s};
            MyString s3{std::move(s1)};
            // s1 is empty now
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                s1 = std::move(*s2);
                // dtor of moved-from must not cause any memory release
                s2->~MyString();
            }
        }
        // now all acquired resources should be released

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
    CASE("Test move assignment of moved from")
    {
        AllocTracerMock tracer;
        AllocMock testAlloc;
        { // we need another scope for memory management
            std::byte buf[sizeof(MyString)];
            MyString *tms = nullptr;

            MyString s1("Text1"s);
            MyString s2{std::move(s1)};
            MyString s3;
            tms = new (buf) MyString{std::move(s3)};
            s3 = std::move(*tms);

            // move moved-from s1 again
            {
                FORBID_CALL(tracer, trackNew(_, _));
                FORBID_CALL(tracer, trackDelete(_));

                TraceSetter ts{testAlloc, &tracer};

                s1 = std::move(s3);
                tms->~MyString();
            }
        }
        // now all acquired resources should be released

        EXPECT(testAlloc.ok());
        EXPECT(testAlloc.getCount() == 0);
    },
    CASE("Test move self-assignment")
    {
        std::string s{"Hello"s};
        MyString ms{"Hello"s};
        ms = ms;
        std::ostringstream os;
        ms.print(os);
        EXPECT(s == os.str());
    },
#endif /* MYSTRING_MOVE_ASS */
};
} // unnamed namespace
