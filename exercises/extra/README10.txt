Exercise "Library"

 - new branch
 - new project based on 'stdlib'
 - again write tests
 - Write functions that return for the given vector
   - minmax(vect): minimum and maximum value
   - sum(vect): the total sum
   - avg(vect): the average value

 - Write a function delOdd(vect) that removes the odd numbers.

 - Write a function asSet(vect) that copies the values into a set
   and returns it.
 - Write a function delEven(set) that removes the even numbers.
