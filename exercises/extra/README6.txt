Exercise "Functions"

 - new branch, use base1 again
 - now write tests for all functions you implement (except I/O)
 - implement a function checkBounds(value, bound1, bound2)
   that returns true if the value is inside the bounds, false otherwise
 - put your loop that pushes the squares into a vector in a function
   fillSquares(vector, count)
   and pass it the vector you want to fill (including tests)
 - put your loop that prints the vector also in a function
   printVector(std::ostream, vector)
 
