Exercise "Inheritance"

 - new branch
 - use project shop
 - add cd-cart.add to your shop.cc
 - implement the shop example with CDs
 - create class hierarchy
 - change ShopCart to use Product interface
 - implement CD class
 - change Book to derive from Product
