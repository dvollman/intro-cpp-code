Exercise "Development Environment"

 - Create new git branch 'ex1' and switch to it
 - Create new "Makefile" project 'hello' in Eclipse
   based on cpp/intro-cpp-code/exercises/hello
 - Type the "Hello, World!" program into the file hello.cc
 - Build and run the program
