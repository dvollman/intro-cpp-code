Exercise "Rule of Zero (kind of)"

 - new branch
 - based on 'mystr'
 - all tests should still work (change implementation only)
 - use unique_ptr to store the string
 - use default for destructor
 - simplify move operations
 - copy still required
