Exercise "new/delete"

 - new branch
 - new project based on 'mystr'
 - implement a small 'MyString' class
 - it internally holds a 'char[]' dynamically allocated
 - clean it up properly in the destructor
 - required member functions:
   - MyString(std::string)
   - size() (like std::string::size())
   - print(ostream)
 - optional member functions:
   - MyString()
   - set(std::string)
 - use iterator interface of std::string
 - properly modularize
