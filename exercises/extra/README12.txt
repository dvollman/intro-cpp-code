Exercise "Modularization"

 - new branch
 - Separate your shop into single files:
   - book.hh
   - book.cc
   - shopCart.hh
   - shopCart.cc
   - shop.cc

 - Put all implementation into .cc files
 - Protect header files with include guards
 - Put everything (except main) into a namespace

