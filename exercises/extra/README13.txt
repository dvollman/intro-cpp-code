Exercise "Exceptions"

 - new branch
 - work on 'base1'
 - again write tests 'EXPECT_THROWS(checkBounds(-5, 0, -10));'
 - make it a precondition of 'checkBounds(num, lower, upper)'
   that 'lower <= upper'
 - throw a 'std::logic_error' if precondition is not met
 - test it with a test case
 - call it in your 'main()' and catch the exception and print it
 - possibly try to fix the problem and retry
