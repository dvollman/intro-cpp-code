Exercise "Basics and Test"

 - Create new git branch 'ex2' and switch to it
 - Create project 'base1'
 - Write a loop to print the first 10 even numbers (starting with 2)
   You'll need one variable for the value and one for the count
   and operator '%'
 - Write a function square() to return the square of a given value (int only)
 - Write first a test case for your function
 - Write a loop to print the squares of the first 10 even numbers
