Exercise "Class template"

 - new branch
 - new project based on 'graphics'
 - tests in separate project based on 'test-graphics'
 - Make PathShape a template over PathBase.
   Hold now the 'path' by value.
 - Adjust calls in demo.cc accordingly.
