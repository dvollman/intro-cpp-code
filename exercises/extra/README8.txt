Exercise "Lambdas"

 - new branch
 - again, think about tests
 - for the loop that creates the even numbers,
   make it a function evenNums() and give it a lambda
   as parameter for the action to do (and a count)
 - call the function with a lambda that prints the squares
   and one that fills a vector with the squares
   the second needs the vector to be catured by reference
