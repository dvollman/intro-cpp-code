Exercise "Function Templates"

 - new branch
 - make checkBounds a function template (delete existing overloads)
 - your tests should still work
 - add a test for string
 - make printVector() a template function
