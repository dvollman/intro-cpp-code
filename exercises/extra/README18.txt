Exercise "Move"

 - new branch
 - based on 'mystr'
 - more tests
 - now you really need a default constructor
 - add move constructor
 - add move assignment
 - moved-from print() should print nothing
