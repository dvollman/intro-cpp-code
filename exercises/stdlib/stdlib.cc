#include <iostream>
#include <vector>
#include <set>
#include "lest.hpp"

using std::cout;
using namespace std::literals;

namespace
{
std::vector lestArgs = {"-p"s, "-a"s};
IntVect vTest1 = {-3};
const lest::test myTests[] =
{
    CASE("Check minmax on single element")
    {
        auto result = introCpp::minmax(vTest1);

        EXPECT(result.first == -3);
        EXPECT(result.second == -3);
    },
};
} // unnamed namespace

int main()
{
    using namespace introCpp;

    int status = lest::run(myTests, lestArgs, std::cout);
    if (status == 0)
    {
        cout << "Tests successful\n";
    } else {
        cout << "Tests failed\n";
    }


    IntVect vi = { 22, 33, 44, 12, 2, 18, 4, 27, 44, 91 };

    cout << "Original vector: ";
    printCont(vi);

    auto mm = minmax(vi);
    cout << "Minimum: " << mm.first << ", Maximum: " << mm.second << '\n';

    cout << "Total: " << sum(vi) << '\n';
    
    cout << "Average: " << avg(vi) << '\n';

    std::set<int> si = asSet(vi);

    delOdd(vi);
    cout << "Vector without odd numbers: ";
    printCont(vi);

    cout << "Original set:\n";
    printCont(si);

    delEven(si);
    cout << "Set without even numbers: ";
    printCont(si);

    return 0;
}
