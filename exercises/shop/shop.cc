// Shop main

#include <vector>
#include <iostream>

using std::cout;

Book b1(101, "Bjarne Stroustrup", "The C++ Programming Language",
        "AWL", "0-201-88954-4", 34.28, 0.675);
Book b2(121, "Scott Meyers", "Effective C++",
        "AWL", "0-201-92488-9", 28.95, 0.355);
Book b3(141, "Herb Sutter", "Exceptional C++",
        "AWL", "0-201-61562-2", 31.70, 0.405);
Book b4(171, "Stan Lippman, Josee Lajoie", "C++ Primer",
        "AWL", "0-201-82470-1", 22.35, 0.829);

typedef std::vector<Book const *> CatalogueT;
CatalogueT catalogue = { &b1, &b2, &b3, &b4 };


int main()
{
    ShopCart sc;

    sc.add(catalogue[0]);
    sc.add(catalogue[2]);
    sc.add(catalogue[2]);

    sc.showList(cout);
    sc.setShippingMethod(ShopCart::airmail);
    cout << "The price total is: " << sc.getPrice() << '\n';

    return 0;
}
