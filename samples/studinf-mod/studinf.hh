// Modularization
#pragma once

#ifndef CPP_SAMPLE_STUD_INF_HH
#define CPP_SAMPLE_STUD_INF_HH
#include <vector>
#include <string>

namespace cppSample
{
class StudentInfo
{
public:
    bool read();
    double grade() const;
    void print() const;

    typedef std::vector<double> HWorkLst;

private:
    void readHW();

    std::string name;
    double midterm, final;
    HWorkLst homework;
};
} // namespace cppSample
#endif // CPP_SAMPLE_STUD_INF_HH
