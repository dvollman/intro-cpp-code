// Modularization

#include "studinf.hh"

#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <vector>
#include <string>
#include <algorithm>

namespace
{

typedef cppSample::StudentInfo::HWorkLst HWorkLst;

double median(HWorkLst hw)
{
    HWorkLst::size_type size = hw.size();
    if (size == 0)
    {
        throw std::runtime_error("median of an empty vector");
    }

    std::sort(hw.begin(), hw.end());

    HWorkLst::size_type mid = size/2;

    if (size % 2 == 0)
    {
        return (hw[mid] + hw[mid+1]) / 2;
    }
    else
    {
        return hw[mid];
    }
}
} // unnamed namespace

namespace cppSample
{

using std::cout;
using std::cin;
using std::endl;
using std::string;

bool StudentInfo::read()
{
    // ask for and read the student's name
    cout << "Please enter first name ('-' for end): ";
    cin >> name;
    if (name == "-")
    {
        return false;
    }

    // ask for and read the midterm and final grades
    cout << "Please enter midterm and final exam grades: ";
    cin >> midterm >> final;

    // ask for the homework grades
    cout << "Enter all homework grades, followed by 0: ";
    // read the homework grades
    readHW();

    return true;
}

double StudentInfo::grade() const
{
    return 0.2 * midterm + 0.4 * final + 0.4 * median(homework);
}

void StudentInfo::print() const
{
    cout << "Name: " << name;
    cout << "   Grade: " << grade() << endl;
}

} // namespace cppSample

void cppSample::StudentInfo::readHW()
{
    while (true)
    {
        double grade;

        cin >> grade;
        if (!cin)
        {
            throw std::runtime_error("Wrong input");
        }
        else if (grade == 0)
        {
            break;
        }

        homework.push_back(grade);
    }
}

