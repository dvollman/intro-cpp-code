// Modularization
#include <iostream>
#include <stdexcept>
#include <vector>
#include "studinf.hh"

using cppSample::StudentInfo;

int main()
{
    typedef std::vector<StudentInfo> StudLst;

    try
    {
        StudLst students;
        StudentInfo rec;

        while (rec.read())
        {
            students.push_back(rec);
        }

        for (StudLst::iterator s = students.begin();
             s != students.end();
             ++s)
        {
            s->print();
        }
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    return 0;
}

