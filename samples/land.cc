// excercise example using C++ standard library

#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <iterator>

int main()
{
    using std::set;
    using std::string;
    using std::ifstream;
    using std::copy;
    using std::inserter;
    using std::istream_iterator;
    using std::ostream_iterator;
    using std::cout;
    using std::endl;

    set<string> tokens;
    ifstream infile("land.dat");

    int count;
    infile >> count; // ignored

    copy(istream_iterator<string>(infile),
         istream_iterator<string>(),
         inserter(tokens, tokens.begin()));
    copy(tokens.begin(), tokens.end(),
         ostream_iterator<string>(cout, " "));
    cout << endl;

    return 0;
}
