// example functions

#include <vector>
#include <string>
#include <iostream>

using namespace std::literals;

namespace introSamples
{
struct PersMap {
    std::string first;
    std::string middle;
    std::string last;

    int id;
};

typedef std::vector<PersMap> PMVect;

PersMap makePM(std::string const &first,
               std::string const &middle,
               std::string const &last,
               int id)
{
    return {first, middle, last,id};
}

void printPersMap(PersMap const &p, std::ostream &os = std::cout)
{
    os << p.id << ": "
       << p.first << ' '
       << p.middle << ' '
       << p.last << '\n';
}

void fillPersMapVector(PMVect &pmv)
{
    pmv.push_back(makePM("Mr."s, "Superuser"s, "Root"s, 0));
    pmv.push_back(makePM("Postman"s, ""s, "Mail"s, 8));
    pmv.push_back(makePM("Filer"s, "T."s, "Ftp"s, 14));
    pmv.push_back(makePM("Anonymous"s, "X."s, "Nobody"s, 99));
}

void printPersMapVector(PMVect const &pmv, std::ostream &os = std::cout)
{
    for (PersMap const &p: pmv)
    {
        printPersMap(p, os);
    }
}
} // namespace introSamples

int main()
{
    introSamples::PMVect sysUsers;
    introSamples::fillPersMapVector(sysUsers);
    introSamples::printPersMapVector(sysUsers);

    return 0;
}
