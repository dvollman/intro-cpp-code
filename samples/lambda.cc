/*
 * Copyright (c) 2014-2017 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <functional>
#include <iostream>
#include <vector>

using std::cout;

float runFoo(std::function<float()> f)
{
    return f();
}

void forEachInIntVect(std::vector<int> &v, std::function<void(int &)> f)
{
    for (int &val: v)
    {
        f(val);
    }
}

void raise(std::vector<int> &v, double percentage)
{
    forEachInIntVect(v,
                     [percentage] (int &val)
                     {
                         double add = val * percentage / 100;
                         val += add;
                     });
}

void handleData()
{
    int faults = 0;
    int samples = 0;
    auto faultRate = [&] {
        return float(faults) / samples;
    };
// ...
    cout << "Current fault rate: "
         << faultRate() << '\n';

    runFoo(faultRate);
    runFoo([=] { return faultRate(); });
}

void plot(std::function<float(float)> foo)
{
    for (float x = -10; x <= 10; x += 1)
    {
        float y = foo(x);
        cout << "next point: (" << x << ',' << y << ")\n";
    }
}

void squareGraph()
{
    plot([] (float x) { return x * x; });
}


int main()
{
    std::vector<int> v{27653, 38913, 19933, 42478};
    forEachInIntVect(v,
                     []
                     (int val)
                     {
                         cout << val << ' ';
                     });
    cout << '\n';

    raise(v, 1.5);
    forEachInIntVect(v,
                     []
                     (int val)
                     {
                         cout << val << ' ';
                     });
    cout << '\n';

    squareGraph();

    return 0;
}
