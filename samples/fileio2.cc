// string
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include <iterator>


using std::cout;
using std::cerr;
using std::ifstream;
using std::istream_iterator;
using std::ofstream;
using std::ostream_iterator;
using std::endl;
using std::string;
using std::getline;

struct MyFileError : std::runtime_error   // specific error class
{
    //using std::runtime_error::runtime_error; // looks strange, but ok
    typedef std::runtime_error BaseError;
    using BaseError::BaseError;
};

int main()
{
    try
    {
        string fileName("land.dat");
        ifstream inFile(fileName);
        if (!inFile)
        {
            throw MyFileError("Couldn't open input file");
        }
        inFile.exceptions(std::ios::badbit);

        int unusedCount;
        inFile >> unusedCount;

        std::vector<string> tokens;

        copy(istream_iterator<string>(inFile),
             istream_iterator<string>(),
             inserter(tokens, tokens.begin()));
        inFile.close();
        inFile.clear();

        sort(tokens.begin(), tokens.end());
        tokens.erase(unique(tokens.begin(), tokens.end()), tokens.end());

        copy(tokens.begin(), tokens.end(),
             ostream_iterator<string>(cout, " "));
        cout << endl;
    }
    catch (MyFileError &e)
    {
        std::cerr << "File error: " << e.what() << endl;
        return 1;
    }
    catch (std::exception &e)
    {
        std::cerr << "Some exeception I don't know: " << e.what() << endl;
        exit(2);
    }
    catch (...)
    {
        std::cerr << "got completely unkown exception" << endl;
        throw;
    }

    return 0;
}

