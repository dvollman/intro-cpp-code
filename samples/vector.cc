// showing some vector usage

#include <cassert>
#include <vector>
#include <string>
#include <type_traits>

using namespace std::literals;
using std::vector;
using std::string;



int main()
{
    // a vector with int
    vector<int> vi1;
    assert(vi1.size() == 0);

    vi1.push_back(15);
    assert(vi1.size() == 1);
    assert(vi1[0] = 15);

    // another int vector
    vector vi2 = { 1, 2, 3, 4 };
    assert(vi2.size() == 4);

    // a vector with strings
    vector<string> vs1 = { "Hello", "World" };
    static_assert(std::is_same_v<std::string, std::decay_t<decltype(vs1[0])>>);
    assert(vs1.size() == 2);

    // not a vector with strings
    vector vx2 = { "Hello", "World" };
    static_assert(!std::is_same_v<std::string, std::decay_t<decltype(vx2[0])>>);
    static_assert(std::is_same_v<char const *, std::decay_t<decltype(vx2[0])>>);
    assert(vx2.size() == 2);

    // a vector with strings again
    vector vs3 = { "Hello"s, "World"s };
    static_assert(std::is_same_v<std::string, std::decay_t<decltype(vs3[0])>>);
    assert(vs3.size() == 2);
    assert(vs3[0].length() == 5);

    // a vector of ???
    //vector vs4 = { "Hello", "World"s }; // compiler error

    return 0;
}
