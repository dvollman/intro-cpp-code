// string
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdexcept>


using std::cout;
using std::cerr;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::string;
using std::getline;


int main()
{
    try
    {
        string fileName("land.dat");
        ifstream inFile(fileName);
        if (!inFile)
        {
            throw std::runtime_error("Couldn't open input file");
        }
        inFile.exceptions(std::ios::badbit);
        ofstream outFile("out1.dat");

        // copy file linewise
        if (!outFile)
        {
            throw std::runtime_error("Couldn't open output file");
        }
        outFile.exceptions(std::ios::badbit);
        string line;
        while (getline(inFile, line))
        {
            outFile << line << endl;
        }
        inFile.close();
        inFile.clear();
        outFile.close();


        // copy file charwise
        inFile.open(fileName);
        outFile.open("out2.dat");

        char c;
        while (inFile.get(c))
        {
            outFile.put(c);
        }
        inFile.close();
        outFile.close();
    }
    catch (std::exception &e)
    {
        std::cerr << e.what() << endl;
        return 1;
    }

    return 0;
}

