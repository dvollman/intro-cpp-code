// inheritance, base class
#ifndef CPP_SAMPLE_DISPLAY_IF_HH
#define CPP_SAMPLE_DISPLAY_IF_HH

#include <iosfwd>

namespace cppSample
{
class Displayable
{
public:
    virtual ~Displayable() = default;

    virtual void display(std::ostream &) const = 0;
};
} // namespace cppSample
#endif /* CPP_SAMPLE_DISPLAY_IF_HH */

