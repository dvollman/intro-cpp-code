// inheritance sample

#include "framedText.hh"
#include "underlinedText.hh"
#include <iostream>

using std::cout;

using namespace cppSample;

namespace
{

void print(Displayable const &t)
{
    t.display(cout);
}

} // unnamed namespace

int main()
{
    Text t1(5, 'X');
    FramedText t2("");
    UnderlinedText t3("World",'=');

    t2.set("Hello");

    print(t1);
    print(t2);
    print(t3);

    return 0;
}
