// inheritance, base class

#include "text.hh"

#include <iostream>

using std::ostream;
using std::string;

namespace cppSample
{

Text::Text(string const &s)
    : txt(s)
{}

Text::Text(int len, char c)
  : txt(len, c)
{}

void Text::display(ostream &o) const
{
    o << txt << '\n';
}

void Text::set(string const &s)
{
    txt = s;
}

} // namespace cppSample
