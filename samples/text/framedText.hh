// inheritance, derived class
#ifndef CPP_SAMPLE_FRAMED_TEXT_HH
#define CPP_SAMPLE_FRAMED_TEXT_HH

#include "text.hh"

namespace cppSample
{

class FramedText : public Text
{
public:
//    using Text::Text;
    FramedText(std::string const &);
    FramedText(int len, char c);

    void display(std::ostream &) const override;
};

} // namespace cppSample
#endif /* CPP_SAMPLE_FRAMED_TEXT_HH */

