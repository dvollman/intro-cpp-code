// inheritance, derived class

#include "framedText.hh"

#include <iostream>

using std::ostream;
using std::string;

namespace cppSample
{

FramedText::FramedText(string const &s)
    : Text(s)
{}

FramedText::FramedText(int len, char c)
    : Text(len, c)
{}

void FramedText::display(ostream &o) const
{
    string horFrame(txt.length()+2, '-');
    horFrame[0] = '+';
    horFrame[txt.length()+1] = '+';
    o << horFrame << '\n';
    o << '|' << txt << '|' << '\n';
    o << horFrame << '\n';
}

} // namespace cppSample
