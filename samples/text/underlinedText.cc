// inheritance, derived class

#include "underlinedText.hh"

#include <iostream>

using std::ostream;
using std::string;

namespace cppSample
{

UnderlinedText::UnderlinedText(string const &s)
    : Text(s)
    , ul('-')
{}

UnderlinedText::UnderlinedText(string const &s, char underline)
    : Text(s)
    , ul(underline)
{}

UnderlinedText::UnderlinedText(int len, char c)
    : Text(len, c)
    , ul('-')
{}

void UnderlinedText::display(ostream &o) const
{
    Text::display(o);
    string line(txt.length(), ul);
    o << line << '\n';
}

} // namespace cppSample
