// inheritance, derived class
#ifndef CPP_SAMPLE_UNDERLINED_TEXT_HH
#define CPP_SAMPLE_UNDERLINED_TEXT_HH

#include "text.hh"

namespace cppSample
{

class UnderlinedText : public Text
{
public:
    UnderlinedText(std::string const &);
    UnderlinedText(std::string const &, char underline);
    UnderlinedText(int len, char c);

    void display(std::ostream &) const override;

private:
    char ul;
};

} // namespace cppSample
#endif /* CPP_SAMPLE_UNDERLINED_TEXT_HH */

