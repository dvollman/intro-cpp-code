// inheritance, base class
#ifndef CPP_SAMPLE_TEXT_HH
#define CPP_SAMPLE_TEXT_HH

#include "display-if.hh"

#include <string>

namespace cppSample
{

class Text : public Displayable
{
public:
    explicit Text(std::string const &);
    Text(int len, char c);

    void display(std::ostream &) const override;
    void set(std::string const &);

protected:
    std::string txt;
};

} // namespace cppSample
#endif /* CPP_SAMPLE_TEXT_HH */

