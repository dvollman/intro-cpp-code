/*
 * Copyright (c) 2014-2017 Detlef Vollmann, vollmann engineering gmbh
 *
 * Distributed under the Boost Software License, Version 1.0.
 * (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <functional>
#include <iostream>
#include <vector>
#include <array>

using std::cout;
using namespace std::literals;

template <typename T, typename U>
T max(T a, U b)
{
    if (a < b)
    {
        return b;
    } else {
        return a;
    }
}

template <typename T, typename Func>
void forEachInVect(std::vector<T> &v, Func f)
{
    for (T &val: v)
    {
        f(val);
    }
}

template <typename Cont, typename Func>
void forEachInCont(Cont &c, Func f)
{
    for (auto &val: c)
    {
        f(val);
    }
}

template <typename Cont>
void printCont(Cont &c, std::ostream &os = cout)
{
    for (auto &val: c)
    {
        os << val << ' ';
    }
    os << '\n';
}

void raise(std::vector<double> &v, double percentage)
{
    forEachInCont(v,
                  [percentage] (double &val)
                  {
                      double add = val * percentage / 100;
                      val += add;
                  });
}

template <typename Func>
void plot(Func foo)
{
    for (float x = -10; x <= 10; x += 1)
    {
        float y = foo(x);
        cout << "next point: (" << x << ',' << y << ")\n";
    }
}

void squareGraph()
{
    plot([] (float x) { return x * x; });
}

template <typename T, size_t N>
std::array<T, N> makeArray(T val)
{
    std::array<T, N> result;
    for (size_t i = 0; i < N; ++i)
    {
        result[i] = val;
    }
    return result;
}


int main()
{
    std::vector<double> v{27653, 38913, 19933, 42478};
    printCont(v);

    raise(v, 1.5);
    printCont(v);

    std::array<std::string, 5> as
        = {"Hello"s, "from"s, "a"s, "string"s, "array"s};
    printCont(as);

    squareGraph();

    cout << max(5.0, 18) << '\n';

    auto myArray = makeArray<int, 24>(42);
    printCont(myArray);

    return 0;
}
