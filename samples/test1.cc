// test cases for a factorial function
#include <iostream>
#include "lest.hpp"

using std::cout;
using namespace std::literals;

int fact(int n)
{
    int result = 1;
    int count = 1;

    while (count <= n)
    {
        result = result * count;
        count = count + 1;
    }

    return result;
}

const lest::test myTests[] =
{
    CASE("Test factorial with 5")
    {
        int result = fact(5);
        EXPECT(result == 120);
    },
    CASE("Test factorial with 1")
    {
        int result = fact(1);
        EXPECT(result == 1);
    },
    CASE("Test factorial with 0")
    {
        int result = fact(1);
        EXPECT(result == 1);
    },
    CASE("Test factorial with -1")
    {
        [[maybe_unused]]int result = fact(-1);
        EXPECT(true); //result is undefined
    },
};

std::vector args = {"-p"s, "-a"s};

int main()
{
    int status = lest::run(myTests, args, std::cerr);
    if (status == 0)
    {
        cout << "Tests successful\n";
    } else {
        cout << "Tests failed\n";
    }

    return 0;
}
