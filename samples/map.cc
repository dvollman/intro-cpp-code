// Map example
// Copyright (c) 2000-2004,2017 Detlef Vollmann, vollmann engineering gmbh

#include <string>
#include <iostream>
#include <map>

using std::map;
using std::multimap;
using std::make_pair;
using std::cout;
using std::string;


void demoMap()
{
    typedef map<string, unsigned int> ZipList;
    ZipList cities;

    cities["Lausanne"] = 1000;
    cities["Neuchatel"] = 2000;
    cities["Bern"] = 3000;
    cities["Basel"] = 4000;
    cities["Aarau"] = 5000;
    cities["Luzern"] = 6000;
    cities["Chur"] = 7000;
    cities["Zuerich"] = 8000;
    cities["St. Gallen"] = 9000;

    cout << (cities.find("Basel"))->second << '\n';
    cout << cities["Basel"] << '\n';
}

void demoMultiMap()
{
    typedef multimap<string, unsigned int> MultiZipList;
    MultiZipList cities;

    cities.insert(make_pair("Zuerich", 8000));
    cities.insert(MultiZipList::value_type("Buchs", 5033));
    cities.insert(make_pair("Buchs", 6211));
    cities.insert(make_pair("Buchs", 8107));
    cities.insert(make_pair("Buchs", 9470));
    cities.insert(make_pair("Luzern", 6000));
    //cities["Chur"] = 7000; // error

    cout << cities.find("Buchs")->second << '\n';

    for (auto it = cities.lower_bound("Buchs");
         it != cities.upper_bound("Buchs");
         ++it)
    {
        cout << it->second << '\n';
    }
}

int main()
{
    demoMap();
    demoMultiMap();

    return 0;
}
